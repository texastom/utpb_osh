#include <stdio.h> // fprintf(), printf(), stderr, getchar(), perror()
#include <unistd.h> // chdir(), fork(), exec(), pid_t
#include <stdlib.h> // malloc(), realloc(), free(), exit(), execvp(), EXIT_SUCCESS< EXIT_FAILURE
#include <string.h> // strcmp(), strtok()
#include <sys/wait.h> //waitpid and associated macros.


#define MAX_LINE 80 /* The maximum length command */
#define DELIMS " \t\r\n\a"
#define TOK_BUFSIZE 64
#define HISTORY_SIZE 65535
#define MAX_COMMANDS_TO_SHOW 10

 #define min( a,b ) \
   ({ __typeof__ ( a ) _a = ( a ); \
       __typeof__ ( b ) _b = ( b ); \
     _a > _b ? _b : _a; })


/*Function Declarations:*/
int osh_cd(char **args);
int osh_help(char **args);
int osh_exit(char **args);
int osh_history(char **args);
char *osh_read_line(void);
char **osh_split_line(char *line);
int osh_launch(char **args);
int osh_execute(char **args);
int osh_most_recent_history(char **args); 
int osh_execute_last_command( );
int osh_nth_command(char **args);
void update_history(char *line);
void shell_loop(void);

/*Strucure for history function*/
struct shell_history {
	char *command[HISTORY_SIZE];
	int index[HISTORY_SIZE];
	int number_of_commands;
};

struct shell_history *current_history;

int main(int argc, char **argv)
{
	//int should_run = 1; //flag to determine when to exit program; may not neeed this
	
	//Load config files, if any.
	current_history = malloc(sizeof(*current_history));
	current_history -> number_of_commands = 0;
	
	//Run command loop.
	shell_loop();
	
	//Perform any shutdown/cleanup.
	
	return EXIT_SUCCESS;
}

void shell_loop(void){
	char *line;
	char **args;
	int status;
	
	do {
		printf("osh> ");
		line = osh_read_line();
		
		if( strcmp( line, "!!\n" ) != 0 ) {
			// Save all commands except for the history command
			if ( strcmp( line, "history\n" ) != 0 ) {
				update_history(line);
			}

			args = osh_split_line(line);
			status = osh_execute(args);
			free(line);
			free(args);
		} else {
			osh_execute_last_command( );
		}
		
	} while(status);
	
}

char *osh_read_line(void) {
	char *line = NULL;
	size_t bufsize = 0; // have getline allocate a buffer for us
	getline(&line, &bufsize, stdin);
	return line;
}

char **osh_split_line(char *line) {
	int bufsize = TOK_BUFSIZE, position = 0;
	char **tokens = malloc(bufsize * sizeof(char*));
	char *token;

	if (!tokens) {
		fprintf(stderr, "osh: allocation error\n");
		exit(EXIT_FAILURE);
	}

	token = strtok(line, DELIMS);
	while (token != NULL) {
		tokens[position] = token;
		position++;

		if (position >= bufsize) {
			bufsize += TOK_BUFSIZE;
			tokens = realloc(tokens, bufsize * sizeof(char*));
			if (!tokens) {
				fprintf(stderr, "osh: allocation error\n");
				exit(EXIT_FAILURE);
			}
		}

		token = strtok(NULL, DELIMS);
	}
	tokens[position] = NULL;
	return tokens;
}

int osh_launch(char **args)
{
	pid_t pid, wpid;
	int status;

	pid = fork();
	if (pid == 0) {
		// Child process
		if (execvp(args[0], args) == -1) {
			perror("osh");
		}
		exit(EXIT_FAILURE);
	} else if (pid < 0) {
		// Error forking
		perror("osh");
	} else {
		// Parent process - still need to implement & here
		do {
			wpid = waitpid(pid, &status, WUNTRACED);
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
	}

	return 1;
}

/*
	List of builtin commands, followed by their corresponding functions.
 */
 
char *builtin_str[] = {
	"cd",
	"help",
	"exit",
	"history",
	"!!"
};

int (*builtin_func[]) (char **) = {
	&osh_cd,
	&osh_help,
	&osh_exit,
	&osh_history,
	&osh_most_recent_history,
	&osh_nth_command
};

int osh_num_builtins() {
	return sizeof(builtin_str) / sizeof(char *);
}

/*
	Builtin function implementations.
*/
int osh_cd(char **args)
{
	if (args[1] == NULL) {
		fprintf(stderr, "osh: expected argument to \"cd\"\n");
	} else {
		if (chdir(args[1]) != 0) {
			perror("osh");
		}
	}
	return 1;
}

int osh_help(char **args)
{
	int i;
	printf("Stephen Brennan's LSH\n");
	printf("Type program names and arguments, and hit enter.\n");
	printf("The following are built in:\n");

	for (i = 0; i < osh_num_builtins(); i++) {
		printf("	%s\n", builtin_str[i]);
	}

	printf("Use the man command for information on other programs.\n");
	return 1;
}

int osh_exit(char **args)
{
	return 0;
}

/**
 * Executes the most recent command. If there is no most recent command, 
 * this function does nothing.
 *
 * @return Executes the most recent command or returns 1 if no command.
 */
int osh_execute_last_command() {
	if( current_history -> number_of_commands < 1 ) {
		return 1;
	}

	char *most_recent_command = current_history -> command[current_history -> number_of_commands - 1];
	
	char **args = osh_split_line( most_recent_command );

	return osh_execute( args );
}

int osh_execute(char **args)
{
	int i;

	if (args[0] == NULL) {
		// An empty command was entered.
		return 1;
	}

	for (i = 0; i < osh_num_builtins(); i++) {
		if (strcmp(args[0], builtin_str[i]) == 0) {
			return (*builtin_func[i])(args);
		}
	}

	return osh_launch(args);
}

int osh_history(char **args){
	//output history to screen
	int i, command_num;
	char *command_line;
	int total_number_of_commands_in_struct = current_history -> number_of_commands;
	int number_of_commands_to_print = min( total_number_of_commands_in_struct, MAX_COMMANDS_TO_SHOW );

	for(i = total_number_of_commands_in_struct - 1; i >= total_number_of_commands_in_struct - number_of_commands_to_print; i-- ){
		command_num = current_history -> index[i];
		command_line = current_history -> command[i];
		printf("%d %s\n", command_num, command_line);
	}
	
	return 1;
}

int osh_most_recent_history(char **args){
	//in progress
	//execute most recent command from history
	int command_num = current_history -> number_of_commands;
	char *line;
	char **arg;
	
	if(command_num < HISTORY_SIZE){ // fewer than 10 commands stored
		line = current_history -> command[command_num];
	} else { // more than 10 commands issued, latest should be at end of array
		line = current_history -> command[HISTORY_SIZE];
	}
	arg = osh_split_line(line);
	int status = osh_execute(args);
		
	free(line);
	free(arg);

	return status;
}

int osh_nth_command(char **args){
	//execute nth command from history
	//need to add error handling here
	return 1;
}

void update_history(char *line){
	//first check to see how many commands so far. if 10 or more, have to rotate the command and index arrays.
	int command_count = current_history -> number_of_commands;
	
	if (command_count < HISTORY_SIZE){ // This may need to be HISTORY_SIZE - 1 ?
		//fewer than 10 commands so far, do this stuff

		// get rid of newline character
		line[ strcspn( line, "\n") ] = 0;

		// allocate memory for the command
		current_history -> command[command_count] = malloc( strlen( line ) );

		// copy the contents of line into the command array
		strcpy( current_history -> command[command_count], line ); 

		current_history -> index[command_count] = (command_count + 1);
		current_history -> number_of_commands = (command_count + 1);
	} else {
		//more than 10 commands, rotate command and index arrays
		int i;
		for(i = 0; i < HISTORY_SIZE; i++){
			char *shift = current_history -> command[i + 1];
			current_history -> command[i] = shift;
			int index_shift = current_history -> index[i+ 1];
			current_history -> index[i] = index_shift;
		}
		current_history -> command[HISTORY_SIZE] = line;
		current_history -> index[HISTORY_SIZE] = (command_count + 1);
		current_history -> number_of_commands = (command_count + 1);
	}
	
}